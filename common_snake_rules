# vim: set syntax=snakemake:
# Required configuration parameters:
# - out_dir
# - data_file
# - train_reps
# - train_epochs

from pathlib import Path
import munch
import h5py
import math
import os


c = munch.munchify(config)
out_dir_p = Path(c.out_dir)
data_dir = str(Path(c.data_file).parent)
data_file_stem = Path(c.data_file).stem


rule binarize_with_threshold:
   """Binarize input dataset by thresholding according to snakemake configuration."""
   input: data_file = c.data_file
   output: out_file = f'{data_dir}/{data_file_stem}_binthres{{threshold}}.h5'
   run:
      data = h5py.File(input.data_file, 'r')['dataset'][...] > float(wildcards.threshold)
      h5py.File(output.out_file, 'w').create_dataset('data', data=data)


rule train:
   input:
      data = f'{data_dir}/{{input_dataset}}.h5'
   output:
      expand("{{out_dir}}/{{input_dataset}}_heldout{{heldout}}_run{rep}.h5", rep=range(c.train_reps))
   params:
      nreps = c.train_reps,
      epochs = c.train_epochs,
      out_dir = c.out_dir
   shell:
      """
      python ../src/train_nor.py\
         --dataset {input.data}\
         --out_dir {params.out_dir}\
         --heldout_idx {wildcards.heldout}\
         --nreps {params.nreps}\
         --epochs {params.epochs}\
      """


rule select_best_run:
   input: expand("{{out_dir}}/{{input_dataset}}_heldout{{heldout}}_run{rep}.h5", rep=range(c.train_reps))
   output: "{out_dir}/{input_dataset}_heldout{heldout}_bestrun.h5"
   run:
      best_F = -math.inf
      for fname in input:
         F = h5py.File(fname, 'r')['train_F'][-1]
         if F > best_F:
            best_file = fname
            best_F = F
      os.symlink(Path(best_file).name, output[0])


rule roc_curve:
   input:
      data_file = f'{data_dir}/{{input_dataset}}.h5',
      model_par_file = out_dir_p / '{input_dataset}_heldout{heldout}_bestrun.h5',
      labels_file = f'{data_dir}/{data_file_stem}_labels.h5',
      archetypes_file = f'{data_dir}/{data_file_stem}_archetypes.h5'
   output:
      out_dir_p / 'roc_curve_{input_dataset}_heldout{heldout}.png'
   shell:
      """
      python ../src/roc_curve.py\
         --model {input.model_par_file}\
         --data {input.data_file}\
         --labels {input.labels_file}\
         --archetypes {input.archetypes_file}\
         --outfile {output[0]}
      """
