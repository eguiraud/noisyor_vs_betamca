from pathlib import Path
import math
import munch
import h5py
import torch as to
import numpy as np
from collections import defaultdict

configfile: "config.yml"
c = munch.munchify(config)
data_dir = str(Path(c.data_file).parent)
data_file_stem = Path(c.data_file).stem
out_dir_p = Path(c.out_dir)


##### RULES START HERE #####
rule roc_curve_plots:
   input:
      expand(f'{c.out_dir}/roc_curve_{data_file_stem}_binthres{{threshold}}_heldout{{heldout}}.png',\
             threshold=c.bin_thresholds, heldout=c.heldout_indexes),
      f'{c.out_dir}/auc_vs_binthreshold.txt'


rule auc_mean:
   input:
      expand(f'{c.out_dir}/roc_curve_{data_file_stem}_binthres{{threshold}}_heldout{{heldout}}.h5',\
             threshold=c.bin_thresholds, heldout=c.heldout_indexes),
   output:
      out_file = f'{c.out_dir}/auc_vs_binthreshold.txt'
   run:
      out_file = open(output.out_file, 'w')
      print('# bin_threshold\tAUC mean +/- stddev', file=out_file)

      aucs = defaultdict(list)  # map from binarization threshold to list of AUCs per heldout idx
      for fname in input:
         m = re.match('.+_binthres(.+)_.+.h5', fname)
         assert m is not None
         bin_threshold = float(m.group(1))
         aucs[bin_threshold].append(h5py.File(fname, 'r')['auc'][...])

      for threshold in aucs:
         avg_auc = np.mean(aucs[threshold])
         stderr_auc = np.std(aucs[threshold])
         print(f'{threshold}\t{avg_auc:<1.2f} +/- {stderr_auc:<1.2f}', file=out_file)


rule labels:
   input: data_file = '{input_dataset_full_path}.h5'
   output: out_file = '{input_dataset_full_path}_labels.h5'
   run:
      # Hamid's labels (and weights) have a last unit that is always on. Discard it.
      # NOTE: "lables" is not a typo, that's the key in the data_file. Might change in the future.
      labels = h5py.File(input.data_file, 'r')['lables'][:, :-1]
      h5py.File(output.out_file, 'w').create_dataset('labels', data=labels)


rule archetypes:
   input: data_file = '{input_dataset_full_path}.h5'
   output: out_file = '{input_dataset_full_path}_threshold{threshold}_archetypes.h5'
   run:
      # Hamid's weights have a last unit that is always on. Discard it.
      gen_weights = to.FloatTensor(h5py.File(input.data_file, 'r')['W_generative'][:, :-1])
      archetypes = gen_weights.ge(float(wildcards.threshold)).t()  # (H, D)
      h5py.File(output.out_file, 'w').create_dataset('archetypes', data=archetypes)


####################### FROM HERE ON, COPIED FROM COMMON_SNAKE_RULES, TO BE DELETED #######################
# FIXME the archetypes_file in rule roc_curve is different: it depends on the binarization threshold!


rule binarize_with_threshold:
   """Binarize input dataset by thresholding according to snakemake configuration."""
   input: data_file = c.data_file
   output: out_file = f'{data_dir}/{data_file_stem}_binthres{{threshold}}.h5'
   run:
      data = h5py.File(input.data_file, 'r')['dataset'][...] > float(wildcards.threshold)
      h5py.File(output.out_file, 'w').create_dataset('data', data=data)


rule train:
   input:
      data = f'{data_dir}/{{input_dataset}}.h5'
   output:
      expand("{{out_dir}}/{{input_dataset}}_heldout{{heldout}}_run{rep}.h5", rep=range(c.train_reps))
   params:
      nreps = c.train_reps,
      epochs = c.train_epochs,
      out_dir = c.out_dir,
      train_H = c.train_H
   shell:
      """
      python ../src/train_nor.py\
         --dataset {input.data}\
         --out_dir {params.out_dir}\
         --heldout_idx {wildcards.heldout}\
         --nreps {params.nreps}\
         --epochs {params.epochs}\
         --n_latents {params.train_H}
      """


rule select_best_run:
   input: expand("{{out_dir}}/{{input_dataset}}_heldout{{heldout}}_run{rep}.h5", rep=range(c.train_reps))
   output: "{out_dir}/{input_dataset}_heldout{heldout}_bestrun.h5"
   run:
      best_F = -math.inf
      for fname in input:
         F = h5py.File(fname, 'r')['train_F'][-1]
         if F > best_F:
            best_file = fname
            best_F = F
      os.symlink(Path(best_file).name, output[0])


rule roc_curve:
   input:
      data_file = f'{data_dir}/{data_file_stem}_binthres{{threshold}}.h5',
      model_par_file = out_dir_p / f'{data_file_stem}_binthres{{threshold}}_heldout{{heldout}}_bestrun.h5',
      labels_file = f'{data_dir}/{data_file_stem}_labels.h5',
      archetypes_file = f'{data_dir}/{data_file_stem}_threshold{{threshold}}_archetypes.h5'
   output:
      out_dir_p / f'roc_curve_{data_file_stem}_binthres{{threshold}}_heldout{{heldout}}.png',
      out_dir_p / f'roc_curve_{data_file_stem}_binthres{{threshold}}_heldout{{heldout}}.h5'
   shell:
      """
      python roc_curve_cafpas.py\
         --model {input.model_par_file}\
         --data {input.data_file}\
         --labels {input.labels_file}\
         --archetypes {input.archetypes_file}\
         --outfile {output[0]}
      """
