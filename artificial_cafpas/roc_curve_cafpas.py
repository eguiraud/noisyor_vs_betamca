# Use a trained NoisyOR model to generate ROC curve datapoints
# Define env variable `ROC_DBG_PRINT=1` for debug printout

# detected_nh = p(s_h = 1 | y^n) >= T
# true positive rate: TPR_h =  sum_n{detected_nh * labels_nh } / sum_n(labels_nh)
# false positive rate: FPR_h = sum_n{detected_nh * (1 - labels_nh)} / (N - sum_n{labels_nh})
# true negative rate: TNR_h = 1 - FPR_h
# because p(not detected | bar not there) = 1 - p(detected | bar not there)

from tvem.models import NoisyOR
from tvem.variational import FullEM, TVEMVariationalStates
import torch as to
import h5py
from argparse import ArgumentParser
import sys
import re
from typing import Tuple
import os
import numpy as np
import matplotlib.pyplot as plt
import math
from pathlib import Path


def get_kfold_testset(tensor: to.Tensor, kfold: Tuple[int, int]) -> to.Tensor:
    """Return slice of `tensor` in its first dimension according to k, heldout_idx.

    :param tensor: Tensor to slice. Shape (N, ...).
    :param kfold: tuple describing the k-folding to use: (k, heldout_idx)
                  where 0 < heldout_idx < k indicates which of the k splits
                  is used as a test set.
    :returns: Tensor with shape (N/k, ...).
    """
    k, selected_split = kfold
    N = tensor.shape[0]
    assert N % k == 0, "Size of dataset not divisible by k -- unsupported"

    fold_size = N // k
    start, end = fold_size * selected_split, fold_size * (selected_split + 1)
    return tensor[start:end]


def detect_components(
    data: to.Tensor, nor: NoisyOR, var_states: TVEMVariationalStates, threshold: float
) -> to.Tensor:
    """Return p(s_h = 1 | data^n) >= threshold."""
    lpj = var_states.lpj
    Kfloat = var_states.K.type_as(lpj).permute(2, 0, 1)
    detected_hn = nor._mean_posterior(Kfloat, lpj) >= threshold
    return detected_hn.t()  # (N, H)


def tpr_and_fpr(data, labels, model, var_states, threshold):
    detected = detect_components(data, model, var_states, threshold)  # (N, H)

    N = data.shape[0]
    n_with_component_h = labels.sum(dim=0, dtype=to.float)
    # TPR_h =  sum_n{detected_nh * labels_nh } / sum_n(labels_nh)
    true_pos_rate = (detected * labels).sum(dim=0, dtype=to.float) / n_with_component_h
    # FPR_h = sum_n{detected_nh * (1 - labels_nh)} / (N - sum_n{labels_nh})
    false_pos_rate = (detected * (1 - labels)).sum(dim=0, dtype=to.float) / (N - n_with_component_h)

    is_zero = (~labels.any(dim=1)) # (N,)
    n_zeros = is_zero.sum(dim=0).item() # (0,)
    detected_zero = ~detected.any(dim=1)  # (N,)
    true_pos_rate_zero = (detected_zero * is_zero).sum(dim=0, dtype=to.float) / n_zeros
    false_pos_rate_zero = (detected_zero * ~is_zero).sum(dim=0, dtype=to.float) / (N - n_zeros)

    if "ROC_DBG_PRINT" in os.environ:
        print(f"\n\n\n******threshold:{threshold}*******")
        sq_size = int(math.sqrt(data.shape[1]))
        for n in range(N):
            datapoint = data[n].reshape(sq_size, sq_size)
            detected_h = to.nonzero(detected[n])
            labels_h = to.nonzero(labels[n])
            W = model.theta["W"].t().reshape(-1, sq_size, sq_size)
            active_gen_fields = W[detected_h]
            labeled_gen_fields = W[labels_h]
            print(f"datapoint:\n{datapoint}\n")
            print(f"active gen. fields:\n{active_gen_fields > 0.5}\n")
            print(f"labelled gen. fields\n{labeled_gen_fields > 0.5}\n\n\n\n")

    return true_pos_rate, false_pos_rate, true_pos_rate_zero, false_pos_rate_zero


def get_test_data(data_fname: str, kfold: Tuple[int, int]) -> to.Tensor:
    f = h5py.File(data_fname, "r")
    return get_kfold_testset(to.ByteTensor(f["data"]), kfold)


def get_ordered_weight_idxs(
    labels_fname: str, archetypes_fname: str, kfold: Tuple[int, int], nor: NoisyOR, 
) -> to.Tensor:
    """Return indexes that reorder weights to match labels/archetypes.

    :param labels_fname: name of HDF5 file containing the "labels" key,
                         a boolean array of shape (N, H)
    :param archetypes_fname: name of HDF5 file containing the "archetypes" key,
                             a boolean array of shape (D, H)
    :param kfold: tuple describing the k-folding to use: (k, heldout_idx)
                  where 0 < heldout_idx < k indicates which of the k splits
                  to use as a test set.
    :param nor: trained NoisyOR model
    :returns: the reordered labels and a "is_all_good" flag

    This function uses archetypes to reorder labels so they fit the learned dictionaries.
    The "archetypes" are H datapoints for which p(s_h = 1| y_h) >> p(s_!h = 1 | y_h), i.e.
    datapoints that represent the archetypical output of singleton states s_h = 1, s_!h = 0,
    with the H latent dimensions ordered the same way they are ordered in the labels.
    True labels might have values that are shuffled w.r.t. the learned dictionaries.
    Use this method to reorder the label values in the latent dimension so that the
    new latent dimensions correspond to the ones that have been learned.
    """
    labels = get_kfold_testset(to.ByteTensor(h5py.File(labels_fname, "r")["labels"]), kfold)
    archetypes = to.ByteTensor(h5py.File(archetypes_fname, "r")["archetypes"])  # (H, D)

    N = archetypes.shape[0]
    H = labels.shape[1]
    assert (
        N == H
    ), f"shapes of labels {tuple(labels.shape)} and archetypes {tuple(archetypes.shape)} do not match"

    var_states = FullEM({"N": N, "H": H, "precision": nor.precision})
    var_states.update(to.arange(N), archetypes, nor.log_pseudo_joint)
    lpj = var_states.lpj
    Kfloat = var_states.K.type_as(lpj).permute(2, 0, 1)

    # max_post_idx_h indicates which latent of the trained model activates
    # archetype number h (which corresponds to label number h in the original labelling).
    # if max_post_idx = (2,0,1) it means that original labelling (a,b,c) must be transformed to (b,c,a)
    # (value at index i indicates the _new_ position that label should take in the reordered labels)
    is_all_good = True
    max_post_idx = to.max(nor._mean_posterior(Kfloat, lpj), dim=0)[1]  # (N==H,)
    assert max_post_idx.shape == (H,)
    if not to.sort(max_post_idx)[0].equal(to.arange(H)):
        is_all_good = False
        print("***WARNING***: Two or more archetypes have max_h{p(s_h | archetype)} with same h.", file=sys.stderr)

    return max_post_idx, is_all_good  # (N, H)


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("--model", required=True, help="HDF5 file with NoisyOR training output.")
    parser.add_argument(
        "--data", required=True, help="HDF5 file with the test dataset at key 'data'."
    )
    parser.add_argument("--outfile", required=True, help="Name of output file")
    parser.add_argument(
        "--labels",
        required=True,
        help="HDF5 file with the labels at key 'labels'. Assumed to be an array of shape (N,H).",
    )
    parser.add_argument(
        "--archetypes",
        required=True,
        help="""HDF5 file with "archetypical" datapoints, i.e. datapoints corresponding with high
                probability to one-hot labels. Assumed to be an array of shape (D, H)
                where H has the same ordering as the labels in the file indicated by --labels.""",
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    model_param_fname = args.model
    data_fname = args.data
    labels_fname = args.labels
    archetypes_fname = args.archetypes

    m = re.match(r"(.+)_heldout([0-9]+).+\.h5", model_param_fname)
    assert m is not None, f"model parameter file '{model_param_fname}' must contain heldout index!"
    out_fname_prefix = Path(m.group(1)).name
    out_dir = Path(m.group(1)).parent
    k, heldout_idx = 10, int(m.group(2))
    kfold = (k, heldout_idx)
    out_fname_stem = f'{out_dir}/roc_curve_{out_fname_prefix}_heldout{heldout_idx}'

    data = get_test_data(data_fname, kfold)
    N = data.shape[0]

    theta = h5py.File(model_param_fname, "r")["theta"]
    pi = to.FloatTensor(theta["pies"][-1])
    W = to.FloatTensor(theta["W"][-1])
    D, H = W.shape
    nor = NoisyOR(N=N, D=D, H=H, W_init=W, pi_init=pi)
    var_states = FullEM({"N": N, "H": H, "precision": nor.precision})
    # update var_states.lpj (must loop until convergence if not FullEM)
    var_states.update(to.arange(var_states.K.shape[0]), data, nor.log_pseudo_joint)

    w_idxs, is_all_good = get_ordered_weight_idxs(labels_fname, archetypes_fname, kfold, nor)
    nor.theta['pies'][:] = nor.theta['pies'][w_idxs]  # reorder nor priors so they match the labels
    nor.theta['W'][:] = nor.theta['W'][:, w_idxs]  # reorder nor weights so they match the labels
    var_states.update(to.arange(var_states.K.shape[0]), data, nor.log_pseudo_joint)

    labels = get_kfold_testset(to.ByteTensor(h5py.File(labels_fname, "r")["labels"]), kfold)
    n_curve_points = 101
    tpr = np.empty(n_curve_points)
    fpr = np.empty_like(tpr)
    tpr_h = np.empty((H, n_curve_points))
    fpr_h = np.empty_like(tpr_h)
    tpr_zero = np.empty_like(tpr)
    fpr_zero = np.empty_like(tpr_zero)
    for i, threshold in enumerate(to.linspace(0, 1, n_curve_points).double()):
        tpr_h[:, i], fpr_h[:, i], tpr_zero[i], fpr_zero[i] = tpr_and_fpr(data, labels, nor, var_states, threshold)
        tpr[i], fpr[i] = tpr_h.mean(), fpr_h.mean()
    sort_idx = fpr.argsort()
    auc = np.trapz(tpr[sort_idx], x=fpr[sort_idx])

    # generate plot
    line_style = '-' if is_all_good else 'r--'
    plt.plot(fpr, tpr, line_style)
    plt.xlabel(r"$<$FPR$>_h$")
    plt.ylabel(r"$<$TPR$>_h$")
    plt.title(f"ROC curve for NoisyOR on {data_fname}")
    plt.annotate(f"AUC: {auc:<1.2f}", xy=(.80,.80), xycoords='axes fraction')
    ax = plt.gca()
    ax.set_xlim((-0.1, 1.1))
    ax.set_ylim((-0.1, 1.1))
    plt.savefig(out_fname_stem + '.png')

    # generate HDF5 file
    out_file = h5py.File(out_fname_stem + '.h5', 'w')
    out_file.create_dataset('tpr_h', data=tpr_h)
    out_file.create_dataset('fpr_h', data=fpr_h)
    out_file.create_dataset('tpr_zero', data=tpr_zero)
    out_file.create_dataset('fpr_zero', data=fpr_zero)
    out_file.create_dataset('auc', data=auc)
