import h5py
import numpy as np
import matplotlib.pyplot as plt

out_dir = "roc_cafpas_per_latent"

for threshold in [0.3, 0.45, 0.5, 0.55, 0.6]:
    tpr_h_mean = np.zeros((2, 101))
    fpr_h_mean = np.zeros((2, 101))
    tpr_zeros_mean = np.zeros((101,))
    fpr_zeros_mean = np.zeros((101,))
    out_fname = f"{out_dir}/tpr_and_fpr_artificialCAFPAs_binthres{threshold}.h5"
    for heldout_idx in range(10):
        fname = f"{out_dir}/roc_curve_artificialCAFPAs_binthres{threshold}_heldout{heldout_idx}.h5"
        f = h5py.File(fname, 'r')
        tpr_h_mean += f['tpr_h'][...]
        fpr_h_mean += f['fpr_h'][...]
        tpr_zeros_mean += f['tpr_zero'][...]
        fpr_zeros_mean += f['fpr_zero'][...]
    tpr_h_mean /= 10
    fpr_h_mean /= 10
    tpr_zeros_mean /= 10
    fpr_zeros_mean /= 10
    out_f = h5py.File(out_fname, 'w')
    out_f.create_dataset('tpr_h', data=tpr_h_mean)
    out_f.create_dataset('fpr_h', data=fpr_h_mean)
    out_f.create_dataset('tpr_zero', data=tpr_zeros_mean)
    out_f.create_dataset('fpr_zero', data=fpr_zeros_mean)
    out_f.close()

    plt.figure()
    plt.plot(fpr_h_mean[0], tpr_h_mean[0])
    plt.title(f'h=0, threhsold {threshold}')
    plt.savefig(out_fname.replace('.h5','_H0.png'))

    plt.figure()
    plt.plot(fpr_h_mean[1], tpr_h_mean[1])
    plt.title(f'h=1, threhsold {threshold}')
    plt.savefig(out_fname.replace('.h5','_H1.png'))

    plt.figure()
    plt.plot(fpr_zeros_mean, tpr_zeros_mean)
    plt.title(f's_h=(0,0), threhsold {threshold}')
    plt.savefig(out_fname.replace('.h5','_zeros.png'))
