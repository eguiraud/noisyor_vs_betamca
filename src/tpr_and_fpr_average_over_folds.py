import h5py
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from glob import glob
import re


def parse_args():
    p = ArgumentParser()
    p.add_argument("--dir")
    p.add_argument("--data_file_stem")
    p.add_argument("--threshold", type=float)
    return p.parse_args()


def avg_per_threshold(threshold: float, out_dir: str, data_file_stem: str):
    all_tpr_h = []
    all_fpr_h = []
    all_tpr_zero = []
    all_fpr_zero = []
    out_fname = f"{out_dir}/tpr_and_fpr_{data_file_stem}_binthres{threshold}.h5"
    in_files = glob(f"{out_dir}/roc_curve_{data_file_stem}_binthres{threshold}_heldout*.h5")
    for fname in in_files:
        f = h5py.File(fname, 'r')
        all_tpr_h.append(f['tpr_h'][...])
        all_fpr_h.append(f['fpr_h'][...])
        all_tpr_zero.append(f['tpr_zero'][...])
        all_fpr_zero.append(f['fpr_zero'][...])
    tpr_h_mean = np.nanmean(all_tpr_h, axis=0)
    tpr_h_std = np.nanstd(all_tpr_h, axis=0)
    fpr_h_mean = np.nanmean(all_fpr_h, axis=0)
    fpr_h_std = np.nanstd(all_fpr_h, axis=0)
    tpr_zeros_mean = np.nanmean(all_tpr_zero, axis=0)
    tpr_zeros_std = np.nanstd(all_tpr_zero, axis=0)
    fpr_zeros_mean = np.nanmean(all_fpr_zero, axis=0)
    fpr_zeros_std = np.nanstd(all_fpr_zero, axis=0)
    out_f = h5py.File(out_fname, 'w')
    out_f.create_dataset('tpr_h', data=tpr_h_mean)
    out_f.create_dataset('tpr_h_err', data=tpr_h_std)
    out_f.create_dataset('fpr_h', data=fpr_h_mean)
    out_f.create_dataset('fpr_h_err', data=fpr_h_std)
    out_f.create_dataset('tpr_zero', data=tpr_zeros_mean)
    out_f.create_dataset('tpr_zero_err', data=tpr_zeros_std)
    out_f.create_dataset('fpr_zero', data=fpr_zeros_mean)
    out_f.create_dataset('fpr_zero_err', data=fpr_zeros_std)
    out_f.close()

    for h in range(len(fpr_h_mean)):
        plt.figure()
        plt.plot(fpr_h_mean[h], tpr_h_mean[h])
        for f, t in zip(all_fpr_h, all_tpr_h):
            plt.plot(f[h], t[h], '--')
        sort_idx = fpr_h_mean[h].argsort()
        auc = np.trapz(tpr_h_mean[h][sort_idx], x=fpr_h_mean[h][sort_idx])
        plt.title(f'h={h}, threshold {threshold}')
        plt.annotate(f"AUC: {auc:<1.2f}", xy=(.80,.80), xycoords='axes fraction')
        plt.savefig(out_fname.replace('.h5',f'_H{h}.svg'))

    plt.figure()
    plt.plot(fpr_zeros_mean, tpr_zeros_mean)
    for f, t in zip(all_fpr_zero, all_tpr_zero):
        plt.plot(f, t, '--')
    sort_idx = fpr_zeros_mean.argsort()
    auc = np.trapz(tpr_zeros_mean[sort_idx], x=fpr_zeros_mean[sort_idx])
    plt.title(f's_h=(0,0), threshold {threshold}')
    plt.annotate(f"AUC: {auc:<1.2f}", xy=(.80,.80), xycoords='axes fraction')
    plt.savefig(out_fname.replace('.h5','_zeros.svg'))


if __name__ == "__main__":
    args = parse_args()
    avg_per_threshold(threshold=args.threshold, out_dir=args.dir, data_file_stem=args.data_file_stem)
