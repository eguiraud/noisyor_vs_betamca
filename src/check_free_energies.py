# Extract max free energy and avg free energy information from files
# Usage: python check_free_energies.py <dir>

import h5py
from glob import glob
import numpy as np
from typing import Dict, List
import math
from collections import defaultdict
import sys
from argparse import ArgumentParser
import re


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("--result_dir", required=True)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    result_dir = parse_args().result_dir
    files = glob(f"{result_dir}/*.h5")

    max_F: Dict[float, float] = {}  # dict of max F per threshold
    all_F: Dict[float, List[float]] = defaultdict(list)  # dict of Fs per threshold
    best_runs: Dict[float, int] = {}  # dict of best run number per threshold

    n_chars_in_prefix = len(f"{result_dir}/nor_bars_threshold")

    for fname in files:
        m = re.match(".+_thres(.+)_run([0-9]+).h5", fname)
        threshold = float(m.group(1))
        run_n = int(m.group(2))

        F = h5py.File(fname, "r")["train_F"][-1]

        all_F[threshold].append(F)
        if F > max_F.get(threshold, -math.inf):
            best_runs[threshold] = run_n
            max_F[threshold] = F

    assert max_F.keys() == all_F.keys() == best_runs.keys()
    for threshold in np.sort(list(max_F.keys())):
        print(f"threshold: {threshold}")
        print(f"\tbest run: {best_runs[threshold]}")
        print(f"\tmax F: {max_F[threshold]:<5.3f}")
        print(f"\tavg F: {np.mean(all_F[threshold]):<5.3f}")
