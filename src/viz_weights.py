# Plot learned dictionaries for each of the files in the results/ directory
# Usage: python viz_weights.py <dir>

import matplotlib.pyplot as plt
from glob import glob
import h5py
import sys
from argparse import ArgumentParser
import math


def parse_args():
    parser = ArgumentParser()
    g = parser.add_mutually_exclusive_group(required=True)
    g.add_argument("--dir")
    g.add_argument("--file")
    parser.add_argument("--heldout_idx", default="*")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    heldout_idx = args.heldout_idx

    if args.dir is not None:
        files = glob(f"{args.dir}/*_heldout{heldout_idx}_bestrun.h5")
    else:
        files = (args.file,)

    for fname in files:
        W = h5py.File(fname, "r")["theta"]["W"][-1][...]
        D, H = W.shape
        sq_side = int(math.sqrt(D))
        W = W.T.reshape(H, sq_side, sq_side)
        ncols = 5
        nrows = (H - 1) // ncols + 1  # minimum number of rows required to fit all plots
        fig, ax = plt.subplots(nrows, ncols)
        plt.suptitle(f"last weights in {fname}")
        ax = ax.reshape(-1)
        for i in range(H):
            a = ax[i]
            a.imshow(W[i], vmin=0.0, vmax=1.0, cmap="gray")
            a.set_xticks([])
            a.set_yticks([])
    plt.show()
