import torch as to
from typing import Tuple


def get_kfold_testset(tensor: to.Tensor, kfold: Tuple[int, int]) -> to.Tensor:
    """Return slice of `tensor` in its first dimension according to k, heldout_idx.

    :param tensor: Tensor to slice. Shape (N, ...).
    :param kfold: tuple describing the k-folding to use: (k, heldout_idx)
                  where 0 < heldout_idx < k indicates which of the k splits
                  is used as a test set.
    :returns: Tensor with shape (N/k, ...).
    """
    k, selected_split = kfold
    N = tensor.shape[0]

    fold_size = N // k
    start, end = fold_size * selected_split, fold_size * (selected_split + 1)
    if N % k > 0 and selected_split == k - 1:
        # last split selected and there is a remainder, append to end
        end += N % k
    print(f"test fold for {kfold}: {start}-{end}")
    return tensor[start:end]


def get_kfold_trainset(tensor: to.Tensor, kfold: Tuple[int, int]) -> to.Tensor:
    """Return train set extracted from `tensor` according to k, heldout_idx.

    :param tensor: Tensor to slice. Shape (N, ...).
    :param kfold: tuple describing the k-folding to use: (k, heldout_idx)
                  where 0 < heldout_idx < k indicates which of the k splits
                  is used as a test set.
    :returns: Tensor with shape (N/k, ...).
    """
    k, selected_split = kfold
    N = tensor.shape[0]

    fold_size = N // k
    heldout_start = fold_size * selected_split
    heldout_end = fold_size * (selected_split + 1)
    if N % k > 0 and selected_split == k - 1:
        # last split selected and there is a remainder, append to end
        heldout_end += N % k
    print(f"train fold for {kfold}: 0:{heldout_start}-{heldout_end}:")
    return to.cat((tensor[:heldout_start], tensor[heldout_end:]))
