from tvem.models import NoisyOR
from tvem.exp import Training, ExpConfig, FullEMConfig
import h5py
import numpy as np
import os
from utils import get_kfold_trainset
from typing import Tuple
import torch as to
from argparse import ArgumentParser
import sys
import re
from pathlib import Path
import math


def train_nor(dataset: str, out_fname: str, epochs: int, H: int = None):
    """Train NoisyOR+FullEM on specified dataset."""
    N, D = h5py.File(dataset, "r")["data"].shape
    if H is None:
        H = 2 * int(math.sqrt(D))
    conf = ExpConfig(batch_size=N, output=out_fname)
    nor = NoisyOR(H=H, D=D)
    exp = Training(conf, FullEMConfig(n_latents=H), nor, dataset)
    for e in exp.run(epochs):
        e.print()


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("--dataset", required=True)
    parser.add_argument("--out_dir", default=".")
    parser.add_argument("--heldout_idx", type=int)
    parser.add_argument("--nfolds", type=int, required=True)
    parser.add_argument("--nreps", type=int, default=10)
    parser.add_argument("--epochs", type=int, default=50)
    parser.add_argument("-H", "--n_latents", type=int)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    dataset = args.dataset
    out_dir = args.out_dir
    heldout_idx = args.heldout_idx
    nreps = args.nreps
    output_prefix = os.path.splitext(Path(dataset).name)[0]

    if args.heldout_idx is not None:
        kfold = (args.nfolds, heldout_idx)
        data = get_kfold_trainset(to.ByteTensor(h5py.File(dataset, "r")["data"][...]), kfold)
    else:
        data = to.ByteTensor(h5py.File(dataset, "r")["data"][...])
    dataset_without_heldout = out_dir + "/tmp_train_dataset.h5"
    h5py.File(dataset_without_heldout, "w").create_dataset("data", data=data)

    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    for rep in range(nreps):
        if args.heldout_idx is not None:
            train_output = f"{out_dir}/{output_prefix}_heldout{heldout_idx}_run{rep}.h5"
        else:
            train_output = f"{out_dir}/{output_prefix}_run{rep}.h5"
        train_nor(dataset_without_heldout, out_fname=train_output, epochs=args.epochs, H=args.n_latents)

    os.remove(dataset_without_heldout)
