# Plot free energies
# Usage: python viz_free_energies.py <dir>

import h5py
from glob import glob
import matplotlib.pyplot as plt
import sys
from argparse import ArgumentParser
import re


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("--result_dir", required=True)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    result_dir = parse_args().result_dir
    files = glob(f"{result_dir}/*_heldout*_run*.h5")

    for fname in files:
        m = re.match(".+_heldout([0-9]+)_run([0-9]+).h5", fname)
        heldout_idx = int(m.group(1))
        run_n = int(m.group(2))

        F = h5py.File(fname, "r")["train_F"][...]

        title = f"Free energies for heldout idx {heldout_idx}"
        plt.figure(title)
        plt.plot(F, label=f"run {run_n}")
        plt.title(title)
        plt.legend()

    plt.show()
