from pathlib import Path
import munch
import h5py
import torch as to

configfile: "config.yml"
c = munch.munchify(config)
data_dir = str(Path(c.data_file).parent)
data_file_stem = Path(c.data_file).stem


##### RULES START HERE #####
include: '../common_snake_rules'


rule roc_curve_plots:
   input: expand(f'{c.out_dir}/roc_curve_{data_file_stem}_binthres{{threshold}}_heldout{{heldout}}.png',\
                 threshold=c.bin_thresholds, heldout=c.heldout_indexes)


rule labels:
   input: data_file = '{input_dataset_full_path}.h5'
   output: out_file = '{input_dataset_full_path}_labels.h5'
   run:
      # Hamid's labels (and weights) have a last unit that is always on. Discard it.
      labels = h5py.File(input.data_file, 'r')['labels'][:, :-1]
      h5py.File(output.out_file, 'w').create_dataset('labels', data=labels)


rule archetypes:
   input: data_file = '{input_dataset_full_path}.h5'
   output: out_file = '{input_dataset_full_path}_archetypes.h5'
   run:
      # Hamid's weights have a last unit that is always on. Discard it.
      gen_weights = to.FloatTensor(h5py.File(input.data_file, 'r')['W_generative'][:, :-1])
      archetypes = gen_weights.ge(0.5).t()  # (H, D)
      h5py.File(output.out_file, 'w').create_dataset('archetypes', data=archetypes)
