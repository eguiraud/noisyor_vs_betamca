# Use a trained NoisyOR model to generate ROC curve datapoints
# Generative fields are ordered by decreasing priors in order to match the label ordering.
# Define env variable `ROC_DBG_PRINT=1` for debug printout

# detected_nh = p(s_h = 1 | y^n) > T
# true positive rate: TPR_h =  sum_n{detected_nh * labels_nh } / sum_n(labels_nh)
# false positive rate: FPR_h = sum_n{detected_nh * (1 - labels_nh)} / (N - sum_n{labels_nh})
# true negative rate: TNR_h = 1 - FPR_h
# because p(not detected | bar not there) = 1 - p(detected | bar not there)

from tvem.models import NoisyOR
from tvem.variational import FullEM, TVEMVariationalStates
from tvem.variational._utils import mean_posterior
import torch as to
import h5py
from argparse import ArgumentParser
import sys
import re
from typing import Tuple
import os
import numpy as np
import matplotlib.pyplot as plt
import math
from pathlib import Path

bool_t = (to.FloatTensor() > 0).dtype  # uint8 or bool depending on torch version


def detect_components(
    data: to.Tensor, nor: NoisyOR, var_states: TVEMVariationalStates, threshold: float
) -> to.Tensor:
    """Return p(s_h = 1 | data^n) > threshold."""
    lpj = var_states.lpj
    Kfloat = var_states.K.type_as(lpj)
    detected_nh = mean_posterior(Kfloat, lpj) > threshold
    return detected_nh  # (N, H)


def tpr_and_fpr(data, labels, model, var_states, threshold, detected):
    N = data.shape[0]
    n_with_component_h = labels.sum(dim=0, dtype=to.float)
    # TPR_h =  sum_n{detected_nh * labels_nh } / sum_n(labels_nh)
    true_pos_rate = (detected * labels.to(bool_t)).sum(dim=0, dtype=to.float) / n_with_component_h
    # FPR_h = sum_n{detected_nh * (1 - labels_nh)} / (N - sum_n{labels_nh})
    false_pos_rate = (detected * (1 - labels).to(bool_t)).sum(dim=0, dtype=to.float) / (N - n_with_component_h)

    is_zero = (~labels.any(dim=1)) # (N,)
    n_zeros = is_zero.sum(dim=0).item() # (0,)
    detected_zero = ~detected.any(dim=1)  # (N,)
    true_pos_rate_zero = (detected_zero * is_zero).sum(dim=0, dtype=to.float) / n_zeros
    false_pos_rate_zero = (detected_zero * ~is_zero).sum(dim=0, dtype=to.float) / (N - n_zeros)

    if "ROC_DBG_PRINT" in os.environ and threshold not in [0., 1.]:
        print(f"\n\n\n******threshold:{threshold}*******")
        sq_size = int(math.sqrt(data.shape[1]))
        print(f"detected:\n{detected}")
        print(f"labels:\n{labels}")
        for n in range(N):
            detected_h = to.nonzero(detected[n])
            labels_h = to.nonzero(labels[n])
            if to.equal(detected_h, labels_h):
                continue
            datapoint = data[n].reshape(sq_size, sq_size)
            W = model.theta["W"].t().reshape(-1, sq_size, sq_size)
            active_gen_fields = W[detected_h]
            labeled_gen_fields = W[labels_h]
            print(f"datapoint:\n{datapoint}\n")
            print(f"detected gen. fields:\n{active_gen_fields > 0.5}\n")
            print(f"labelled gen. fields\n{labeled_gen_fields > 0.5}\n\n\n\n")

    return true_pos_rate, false_pos_rate, true_pos_rate_zero, false_pos_rate_zero


def get_test_data(data_fname: str) -> to.Tensor:
    f = h5py.File(data_fname, "r")
    return to.ByteTensor(f["data"][...])


def get_weight_idxs_decr_priors(nor: NoisyOR) -> to.Tensor:
    """Return indexes that reorder generative fields by decreasing prior.

    :param nor: trained NoisyOR model
    """
    idxs = to.LongTensor([0,1])

    priors = nor.theta['pies']
    assert priors.shape == (2,)
    if priors[0] < priors[1]:
        idxs = to.LongTensor([1,0])

    return idxs


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("--model", required=True, help="HDF5 file with NoisyOR training output.")
    parser.add_argument(
        "--data", required=True, help="HDF5 file with the test dataset at key 'data'."
    )
    parser.add_argument("--outfile", required=True, help="Name of output file")
    parser.add_argument(
        "--labels",
        required=True,
        help="HDF5 file with the labels at key 'labels'. Assumed to be an array of shape (N,H).",
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    model_param_fname = args.model
    data_fname = args.data
    labels_fname = args.labels

    out_dir = Path(model_param_fname).parent
    out_fname_prefix = Path(data_fname).stem
    out_fname_stem = f'{out_dir}/roc_curve_{out_fname_prefix}'

    data = get_test_data(data_fname)
    N = data.shape[0]

    theta = h5py.File(model_param_fname, "r")["theta"]
    pi = to.FloatTensor(theta["pies"][-1])
    W = to.FloatTensor(theta["W"][-1])
    D, H = W.shape
    nor = NoisyOR(D=D, H=H, W_init=W, pi_init=pi)

    ordered_idxs = get_weight_idxs_decr_priors(nor)

    nor.theta['pies'][:] = nor.theta['pies'][ordered_idxs]  # reorder nor priors so they match the labels
    nor.theta['W'][:] = nor.theta['W'][:, ordered_idxs]  # reorder nor weights so they match the labels
    var_states = FullEM(N=N, H=H, precision=nor.precision)
    # must loop until convergence if not FullEM
    var_states.update(to.arange(var_states.K.shape[0]), data, nor)

    labels = to.ByteTensor(h5py.File(labels_fname, "r")["labels"][...])

    n_curve_points = 101
    tpr = np.empty(n_curve_points)
    fpr = np.empty_like(tpr)
    detected = to.empty((n_curve_points, N, H), dtype=to.uint8)
    thresholds = to.linspace(0, 1, n_curve_points).double()
    tpr_h = np.empty((H, n_curve_points))
    fpr_h = np.empty_like(tpr_h)
    tpr_zero = np.empty_like(tpr)
    fpr_zero = np.empty_like(tpr_zero)
    for i, threshold in enumerate(thresholds):
        detected[i] = detect_components(data, nor, var_states, threshold)  # (N, H)
        tpr_h[:, i], fpr_h[:, i], tpr_zero[i], fpr_zero[i] = tpr_and_fpr(data, labels, nor, var_states, threshold, detected[i])
        tpr[i], fpr[i] = tpr_h[:,i].mean(), fpr_h[:,i].mean()
    sort_idx = fpr.argsort()
    auc = np.trapz(tpr[sort_idx], x=fpr[sort_idx])

    # generate plot
    line_style = '-'
    plt.plot(fpr, tpr, line_style)
    plt.xlabel(r"$<$FPR$>_h$")
    plt.ylabel(r"$<$TPR$>_h$")
    plt.title(f"ROC curve for NoisyOR on {data_fname}")
    plt.annotate(f"AUC: {auc:<1.2f}", xy=(.80,.80), xycoords='axes fraction')
    ax = plt.gca()
    ax.set_xlim((-0.1, 1.1))
    ax.set_ylim((-0.1, 1.1))
    plt.savefig(out_fname_stem + '.png')

    # generate HDF5 file
    out_file = h5py.File(out_fname_stem + '.h5', 'w')
    out_file.create_dataset('tpr_h', data=tpr_h)
    out_file.create_dataset('fpr_h', data=fpr_h)
    out_file.create_dataset('tpr_zero', data=tpr_zero)
    out_file.create_dataset('fpr_zero', data=fpr_zero)
    out_file.create_dataset('auc', data=auc)
    out_file.create_dataset('inferred_labels', data=detected)
    out_file.create_dataset('roc_thresholds', data=thresholds)
