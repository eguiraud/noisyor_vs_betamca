# NoisyOR vs BetaMCA

Experiments to compare NoisyOR and BetaMCA:
* bars test on dataset with beta noise
* training on CAFPAS

### Usage

```bash
$ conda create env -f env.tml  # install all dependencies from conda
$ conda activate noisyor_vs_betamca  # activate environment
$ cd beta_bars # or another experiment directory
$ # optional: inspect config.yml, the experiment configuration
$ snakemake
```
